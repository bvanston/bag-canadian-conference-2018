For the paper entitled: Automatically Generating and Solving Eternity II Style Puzzles

Authors: Geoff Harris, Bruce James Vanstone, Adrian Gepp

The source code, and supporting project files, are in the \Code directory.

The data files are in subdirectories within the \Data subdirectory.

The data for the META-10 challenge is contained in the \META2010-Instances directory.

Note that the source code contains modules used for testing the correctness of the implementations.  This code has been deliberately left in as the project is ongoing and further research continues.

